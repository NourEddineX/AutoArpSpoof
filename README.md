# AutoArpSpoof
A simple automated bash script to arpspoof all clients on the network 
## Requirements ##

* arpspoof
* iproute2
* net-tools

## How to use it? ##
It's very simple
 
##### Get the script #####

```

git clone https://github.com/NourEddineX/AutoArpSpoof

```
##### Change directory to the script directory #####

```

cd AutoArpSpoof

```
##### Change the script permissions to executable #####
```
chmod +x AutoArpSpoof
```
##### ArpSpoof Everyone :D #####
NOTE: you must run it as root
```
./AutoArpSpoof <network interface> # such as wlan0 and eth0
```
## How to stop it? ##
NOTE: you must run it as root
```
pkill AutoArpSpoof ; pkill arpspoof
```


# And feel free to submit issues! #
